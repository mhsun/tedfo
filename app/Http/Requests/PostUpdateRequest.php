<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:200',
            'body' => 'required',
            'category_id' => 'required|array',
            'image' => 'nullable|image',
        ];
    }

    public function messages()
    {
        return [
            'category_id.required' => 'The category field is required',
            'category_id.array' => 'Invalid data provided for the category field',
        ];
    }
}
