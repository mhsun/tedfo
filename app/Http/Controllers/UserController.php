<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $data['users'] = User::where('role', '<>', User::ROLE_ADMIN)->paginate(5);
        return view('user.all', $data);
    }

    public function showUserPost(User $user)
    {
        $data['posts'] = Post::with('user')->where('user_id', $user->id)->simplePaginate();
        return view('user.post', $data);
    }
}
