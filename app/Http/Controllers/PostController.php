<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    public function _construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['posts'] = Post::with('user', 'category')
                            ->where('publish_time','<=', date('Y-m-d H:ia'))
                            ->orderBy('publish_time')
                            ->simplePaginate();

        $data['categories'] = Category::all();
        return view('post.all', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::all();
        return view('post.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PostCreateRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();

        if ($request->hasFile('image')) {
            $data['image'] = $request->image->store('post');
        }

        $post = Post::create($data);

        $post->category()->sync($request->category_id);

        return redirect('post')->with('success', "Post Created successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $data['post'] = $post;
        $data['categories'] = Category::all();
        return view('post.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Post $post)
    {
        $this->authorize('update', $post);

        $data['post'] = Post::with('user', 'category')->whereId($post->id)->first();
        $data['categories'] = Category::all();
        return view('post.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostUpdateRequest $request
     * @param  \App\Post $post
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(PostUpdateRequest $request, Post $post)
    {
        $this->authorize('update', $post);

        $data = $request->all();

        if (empty($request->publish_time)) {
            unset($data['publish_time']);
        }

        if ($request->hasFile('image')) {
            if (File::exists(public_path('storage/'.$post->image))) {
                File::delete(public_path('storage/'.$post->image));
                $data['image'] = $request->image->store('post');
            }
        }

        $post->update($data);

        $post->category()->sync($request->category_id);

        return redirect()->back()->with('success', 'Post Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Post $post)
    {
        $this->authorize('update', $post);

        if (File::exists(public_path('storage/'.$post->image))) {
            File::delete(public_path('storage/'.$post->image));
        }

        $post->category()->detach($post->id);
        $post->delete();

        return redirect('post', 'success', "Post deleted successfully");
    }

    public function showMyPost()
    {
        $data['posts'] = Post::with('user', 'category')
            ->where('user_id',Auth::id())
            ->orderBy('publish_time')
            ->simplePaginate();

        $data['categories'] = Category::all();
        return view('post.my', $data);
    }
}
