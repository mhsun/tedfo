<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Category|null $category
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category = null)
    {
        if (!empty($category)) {
            $data['editable'] = $category;
        }

        $data['categories'] = Category::with('user')->get();
        return view('category.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Category|null $category
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, Category $category = null)
    {
        $rules['name'] = 'required|unique:categories';

        if (!empty($category)) {
            $rules['name'] = 'required';
        }

        $this->validate($request,$rules);

        $msg = "Category saved";

        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['slug'] = Str::slug($request->name);

        Category::updateOrCreate([
            'name' => $request->name
        ], $data);

        return redirect('category')->with('success', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect('category')->with('success', 'Category deleted');
    }

    public function showPostByCategory($slug)
    {
        $data['posts'] = Category::with('post','post.user')->where('slug', $slug)->first();
        $data['categories'] = Category::all();

        return view('category.post', $data);
    }
}
