<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FooController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSetFooForm()
    {
        return view('foo');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function setFooToSession(Request $request)
    {
        $this->validate($request, [
            'foo' => 'required'
        ], [
            'foo.required' => 'You must set foo to continue'
        ]);

        Session::put('foo', $request->foo);

        return redirect('login');
    }
}
