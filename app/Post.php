<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title',
        'body',
        'image',
        'publish_time',
        'user_id'
    ];

    /*protected $casts = [
        'publish_time' => 'datetime',
    ];*/

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsToMany('App\Category');
    }
}
