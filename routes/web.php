<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::redirect('/', '/post');

Auth::routes();

Route::middleware('foo.set')->group(function () {
    Route::get('/setfoo', 'FooController@showSetFooForm');
    Route::post('/setfoo', 'FooController@setFooToSession');
});

Route::middleware('foo')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::middleware('admin')->group(function () {
        Route::get('category/{category?}', 'CategoryController@index');
        Route::post('category/{category?}', 'CategoryController@store');
        Route::delete('category/{category}', 'CategoryController@destroy');

        Route::get('user', 'UserController@index');
        Route::get('user/post/{user}', 'UserController@showUserPost');
    });

    Route::get('post/category/{slug}', 'CategoryController@showPostByCategory');
    Route::get('post/my', 'PostController@showMyPost');
    Route::resource('post', 'PostController');
});
