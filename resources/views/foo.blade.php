<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Foo</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                @if (auth()->user()->role == \App\User::ROLE_ADMIN)
                    <a href="{{ url('user') }}">User</a>
                    <a href="{{ url('category') }}">Category</a>
                @endif
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">

        @if (session()->get('error'))
            <div class="alert alert-danger text-center">
                <p class="text-danger" style="color: red;">{{ session()->get('error') }}</p>
            </div>
        @endif

        @if (count($errors->all()) > 0)
            <div class="alert alert-danger text-center">
                @foreach($errors->all() as $error)
                    <p class="text-danger" style="color: red;">{{ $error }}</p>
                @endforeach
            </div>
        @endif

        <form method="post">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label><b>Foo:</b></label>
                        <input type="text" name="foo" class="form-control" value="{{ old('foo') }}"
                               placeholder="Type your value here">
                    </div>

                    <br>
                    <input type="submit" value="Submit">
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
