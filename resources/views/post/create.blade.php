@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Create Post</div>
                    @if (session()->get('success'))
                        <div class="alert alert-success text-center">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if (session()->get('error'))
                        <div class="alert alert-danger text-center">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <div class="card-body">
                        <form method="POST" action="{{ route('post.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Title</label>
                                <div class="col-md-8">
                                    <input id="name" type="text"
                                           class="form-control @error('title') is-invalid @enderror" name="title"
                                           value="{{ old('title') }}" required autofocus>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Body</label>
                                <div class="col-md-8">
                                    <textarea id="summernote"
                                              class="form-control @error('body') is-invalid @enderror" name="body"
                                              required>{{ old('body') }}</textarea>

                                    @error('body')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Category</label>
                                <div class="col-md-8">
                                    <select name="category_id[]"
                                            class="form-control @error('category') is-invalid @enderror" multiple>
                                        <option>--Select Category--</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}"
                                            @foreach(old('category_id') ?? [] as $cat)
                                                {{ $cat == $category->id ? 'selected' : '' }}
                                                @endforeach
                                            >{{ $category->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('category')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Image</label>
                                <div class="col-md-8">
                                    <input type="file"
                                           class="form-control @error('image') is-invalid @enderror" name="image"
                                           required>

                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Publish Time</label>
                                <div class="col-md-8">
                                    <input type="datetime-local"
                                           class="form-control @error('publish_time') is-invalid @enderror"
                                           name="publish_time"
                                           value="{{ old('publish_time', date('mm/dd/yyyy H:ia')) }}" required>

                                    @error('publish_time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-9">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
