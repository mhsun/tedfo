@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Update Post</div>
                    @if (session()->get('success'))
                        <div class="alert alert-success text-center">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if (session()->get('error'))
                        <div class="alert alert-danger text-center">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    @if (count($errors->all()) > 0)
                        <div class="alert alert-danger text-center">
                            @foreach($errors->all() as $error)
                                <p class="text-danger" style="color: red;">{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                    <div class="card-body">
                        <form method="POST" action="{{ route('post.update', $post->id) }}"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Title</label>
                                <div class="col-md-8">
                                    <input id="name" type="text"
                                           class="form-control @error('title') is-invalid @enderror" name="title"
                                           value="{{ old('title', $post->title ?? '') }}" required autofocus>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Body</label>
                                <div class="col-md-8">
                                    <textarea id="summernote"
                                              class="form-control @error('body') is-invalid @enderror" name="body"
                                              required>{{ old('body',$post->body ?? '') }}</textarea>

                                    @error('body')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Category</label>
                                <div class="col-md-8">
                                    <select name="category_id[]"
                                            class="form-control @error('category') is-invalid @enderror" multiple>
                                        <option>--Select Category--</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}"
                                            @foreach($post->category as $cat)
                                                {{ $cat->id == $category->id ? 'selected' : '' }}
                                            @endforeach
                                            >{{ $category->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('category')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Image</label>
                                <div class="col-md-8">
                                    <input type="file"
                                           class="form-control @error('image') is-invalid @enderror" name="image">

                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <img src="{{ asset('storage/'.$post->image) }}"
                                         class="img img-thumbnail img-responsive mt-2" alt="post image" height="100px" width="200px">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Publish Time</label>
                                <div class="col-md-8">
                                    <input type="datetime-local"
                                           class="form-control @error('publish_time') is-invalid @enderror"
                                           name="publish_time"
                                           value="{{ old('publish_time') }}">
                                    <b>Currently set on: {{ date('m/d/y H:ia', strtotime($post->publish_time)) }}</b>

                                    @error('publish_time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-9">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
