@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if (session()->get('success'))
                    <div class="alert alert-success text-center">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if (session()->get('error'))
                    <div class="alert alert-danger text-center">
                        {{ session()->get('error') }}
                    </div>
                @endif
            </div>
        </div>
        @if (count($posts) < 1)
            No post to show. <a href="{{ url('post/create') }}">Create One</a>
        @else
            <div class="row">
                <div class="col-md-11"></div>
                <div class="col-md-1">
                    <a href="{{ url('post/create') }}">
                        <button class="btn btn-sm btn-primary">Create New</button>
                    </a>
                </div>
            </div>
        @endif

        <br/>
        @foreach($posts as $post)
            <div class="col-md-12">
                <div class="card mt-3">
                    <div class="card-header">
                        <div style="float: left;" class="mr-5">
                            {{ $post->title ?? 'Unknown' }} by
                            <b>{{ $post->user->id == auth()->id() ? 'You' : $post->user->name }}</b><br>
                            <small>{{ date('d M, Y H:ia', strtotime($post->publish_time)) }}</small>
                            @if ($post->publish_time <= date('Y-m-d H:ia'))
                                <label class="badge badge-success">Published</label>
                            @endif
                            <div>
                                @foreach($post->category as $postCategory)
                                    <label class="badge badge-primary">{{ $postCategory->name }}</label>
                                @endforeach
                            </div>
                        </div>
                        @can('update', $post)
                            <div style="float: left">
                                <a href="{{ url('post/'.$post->id.'/edit') }}" style="float: left" class="mr-1">
                                    <button class="btn btn-sm btn-success">Edit</button>
                                </a>
                                <form method="post" action="{{ url('post', $post->id) }}" style="float: left">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger"
                                            onclick="return confirm('Are you sure to delete?')">Delete
                                    </button>
                                </form>
                            </div>
                        @endcan
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ asset('storage/'.$post->image) }}" class="img img-thumbnail img-responsive"
                                     alt="post image">
                            </div>
                            <div class="col-md-10">
                                @if (strlen($post->body) > 200)
                                    {{ substr($post->body, 0, 200) }}.. <a href="{{ url('post', $post->id) }}"> Show
                                        More</a>
                                @else
                                    {{ $post->body }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row mt-5">
            <div class="col-md-5"></div>
            <div class="col-md-2">{{ $posts->links() }}</div>
            <div class="col-md-4"></div>
        </div>
    </div>
@endsection
