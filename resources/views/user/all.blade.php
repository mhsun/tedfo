@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if (session()->get('success'))
                    <div class="alert alert-success text-center">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if (session()->get('error'))
                    <div class="alert alert-danger text-center">
                        {{ session()->get('error') }}
                    </div>
                @endif
            </div>
        </div>

        <br/>
        <div class="col-md-12 mt-2">
            <div class="card">
                <div class="card-header">User List</div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->name ?? 'Unknown' }}</td>
                                <td>{{ $user->email ?? 'Unknown' }}</td>
                                <td>
                                    <a href="{{ url('user/post', $user->id) }}" style="float: left" class="mr-1">
                                        <button class="btn btn-sm btn-success">Posts</button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-5">
            </div>
            <div class="col-md-6">
                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection
